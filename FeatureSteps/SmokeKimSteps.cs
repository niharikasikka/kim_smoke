﻿using KIM_Automation.Pages;
using KIM_Automation.Pages.KIM_Automation.Pages;
using KIM_SmokeTest.BrowserFactory;
using KIM_SmokeTest.Pages;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_SmokeTest.FeatureSteps
{
    [Binding]
    public class SmokeKimSteps
    {
        //variable for public void ThenIVerifyTheOfTeacherAndCo_Educator, public void ThenICheckTheNumberOfEducatorInOtherEducatorsTab(), public void ThenICheckTheNumberOfTeacherInTeachersTab()
        int eRNoofEducator = 0;
        int eRNoofTeacher = 0;
        string url;
        public IWebDriver Driver { get; private set; }
        string AddedNewService;
        string winHandleBefore;
        [Given(@"I navigate to Kim sharepoint aplication homepage")]
        public void GivenINavigateToKimSharepointAplicationHomepage()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            //Navigating to KIM1 homepage
            Kim1Homepage homePage = new Kim1Homepage(Driver);
            url = homePage.GetUrl();
        }

        [Given(@"I login using internal username and password")]
        public void GivenILoginUsingInternalUsernameAndPassword()
        {

            Kim1Homepage homePage = new Kim1Homepage(Driver);
            homePage.SignIn(url);
          
            //homePage.EnterUsername();
            //homePage.EnterPassword();
        }
       //login as any userid and password
        [Given(@"I login using username and password")]
        public void GivenILoginUsingUsernameAndPassword()
        {
            Kim1Homepage homePage = new Kim1Homepage(Driver);
            homePage.SignIn(url);
        }

        [When(@"I press login button")]
        public void WhenIPressLoginButton()
        {
            //Kim1Homepage homePage = new Kim1Homepage(Driver);
            //homePage.ClickButton();
        }


        [When(@"I am logged in successfully")]
        public void WhenIAmLoggedInSuccessfully()
        {
            Kim1Homepage homePage = new Kim1Homepage(Driver);
            homePage.LoggedIn();
        }

        [When(@"I click on service providers")]
        public void WhenIClickOnServiceProviders()
        {
            Kim1Homepage homePage = new Kim1Homepage(Driver);
            homePage.ServiceProviderClick();

        }

       
        [When(@"I click on service providers for external user")]
        public void WhenIClickOnServiceProvidersForExternalUser()
        {
            ServiceProviderListPage spListPage = new ServiceProviderListPage(Driver);
            spListPage.ServiceProvider_ExternalUser();
        }


        [When(@"I search for an organisation and click on it")]
        public void WhenISearchForAnOrganisationAndClickOnIt()
        {

            ServiceProviderListPage spListPage = new ServiceProviderListPage(Driver);
            string userType = CommonFunctions.GetResourceValue("Kim_UserType");
            if (userType.Equals("internal"))
            {
                spListPage.SearchAndClick();
            }
            else if (userType.Equals("external"))
            {
                spListPage.ServiceProvider_ExternalUser();
            }
        }

        [When(@"I add a new service and fill in details and submit")]
        public void WhenIAddANewServiceAndFillInDetailsAndSubmit(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.AddService();
            AddedNewService = spHomePage.FillForm_NoEym(table);
        }

        [When(@"I add a new EYM service and fill in details and submit")]
        public void WhenIAddANewEYMServiceAndFillInDetailsAndSubmit(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.AddService();
            AddedNewService = spHomePage.FillForm_Eym(table);
        }

        [When(@"I add multiple services and fill in details and submit")]
        public void WhenIAddMultipleServicesAndFillInDetailsAndSubmit(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.AddService();
            AddedNewService = spHomePage.FillForm_NoEym_MultipleServices(table);
        }
        [Then(@"I edit a (.*) for EYM funding")]
        public void ThenIEditAServiceForEYMFunding(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.EditService_EymFunding(service);
        }

        [Then(@"I apply for EYM organisation")]
        public void ThenIApplyForEYMOrganisation()
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.Apply_EymOrg();
        }

        [Then(@"I verify EYM funding status for (.*) is submitted")]
        public void ThenIVerifyEYMFundingStatusForServiceIsSubmitted(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.Verify_EymFunding_Submitted(service);
        }
        [Then(@"I edit a (.*) for transfer")]
        public void ThenIEditAServiceForTransfer(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.EditService_Transfer(service);
        }

        [Then(@"I transfer a service to a different (.*)")]
        public void ThenITransferAServiceToADifferentServiceProvider(string serviceProvider)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.FillForm_TransferService(serviceProvider);
        }

        [Then(@"I verify EYM funding status for service is Approved")]
        public void ThenIVerifyEYMFundingStatusForPondsKidsIsApproved()
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.Verify_EymFunding_Approved();
        }


        [Then(@"I verify that (.*) is transferred")]
        public void ThenIVerifyThatServiceIsTransferred(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.Verify_ServiceTransfer(service);
        }



        [Then(@"I verify that the service is added successfully")]
        public void ThenIVerifyThatTheServiceIsAddedSuccessfully()
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.AddedSuccess();
        }


        [When(@"I go to manage job url and run the services batch job")]
        public void WhenIGoToManageJobUrlAndRunTheServicesBatchJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            CommonFunctions.GetUrl(Driver, "ManageJobs_Url");
            mgHomepage.ManageJobsSignIn();
            try 
            {
                mgHomepage.SearchJobName();
            }
            catch (ElementNotInteractableException)
            {

            }
        }
       
        [When(@"I go to (.*) and job is successfully run")]
        public void WhenIGoToKIMSharePointEnrolmentConfirmationToKIMCRMJobAndJobIsSuccessfullyRun()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            mgHomepage.GoToJob_Verify();
        }

        [Given(@"I go to manage job url")]
        public void GivenIGoToManageJobUrl()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            //Navigate to integrated jobs url
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            string env = CommonFunctions.GetResourceValue("Env");
            //Navigate to the url based on env
            if (env.Equals("dev"))
            {
                //CommonFunctions.GetUrl(Driver, "ManageJobs_dev");
                url = CommonFunctions.GetResourceValue("ManageJobs_dev");

            }
            else if (env.Equals("test"))
            {
                CommonFunctions.GetUrl(Driver, "ManageJobs_test");

            }
            else if (env.Equals("stage"))
            {
                CommonFunctions.GetUrl(Driver, "ManageJobs_stage");

            }
            else if (env.Equals("prod"))
            {
                CommonFunctions.GetUrl(Driver, "ManageJobs_prod");

            }
            string userName;
            string password;
            
                password = CommonFunctions.GetResourceValue("Kim1Password_Internal");
                userName = CommonFunctions.GetResourceValue("Kim1UserId_Internal");
                Driver.Navigate().GoToUrl("https://" + userName + ":" + password + "@" + url);
            
            var actualText = Driver.Title.ToString();
    
            if (actualText.Equals("KIM Integration Service Admin Site")) {
                Driver.FindElement(By.XPath("//div[@class='hpsummary-anchor-text']")).Click();
            }
            else if(actualText.Equals("Manage jobs"))
            {
               //No action required as user is on job list page
            }
            else
            {
                mgHomepage.ManageJobsSignIn();
            }

        }

        [When(@"I run the services batch job")]
        public void WhenIRunTheServicesBatchJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged in
            try
            {
                mgHomepage.SearchJobName();
            }
            catch (ElementNotInteractableException)
            {

                Console.WriteLine("User not logged in. It can be due to multiple reasons");
            }
            
        }
        [When(@"I run the esk wrapper job to import programs, child etc to kim crm")]
        public void WhenIRunTheEskWrapperJobToImportProgramsChildEtcToKimCrm()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged in
            try
            {
                mgHomepage.SearchEskJobName();
            }
            catch (ElementNotInteractableException)
            {
                Console.WriteLine("User not logged in. It can be due to multiple reasons");
            }
        }

        [When(@"I run the ybsk wrapper job to import programs, child etc to kim crm")]
        public void WhenIRunTheYbskWrapperJobToImportProgramsChildEtcToKimCrm()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged in
            try
            {
                mgHomepage.SearchYbskJobName();
            }
            catch(Exception)
            {
                Console.WriteLine("User not logged in. It can be due to multiple reasons");
            }
        }


        [Then(@"the service is successfully run")]
        public void ThenTheServiceIsSuccessfullyRun()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            mgHomepage.ServiceRunSuccess();

        }


        [When(@"I logout of manage jobs")]
        public void WhenILogoutOfManageJobs()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            mgHomepage.LogOut();

        }

        [Given(@"I login to Kim crm application as system admin")]
        public void GivenILoginToKimCrmApplicationAsSystemAdmin()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            string env = CommonFunctions.GetResourceValue("Env");
            //Navigate to the url based on env
            if (env.Equals("dev"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_dev_login");
            }
            else if (env.Equals("test"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_test_login");

            }
            else if (env.Equals("stage"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_stage_login");

            }
            else if (env.Equals("prod"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_prod_login");

            }

            Kim2HomePage kim2homePage = new Kim2HomePage(Driver);
            kim2homePage.SignIn_COSystemAdmin();
            //CommonFunctions.GetUrl(Driver, "Kim2Url_dev");
        }
        [Given(@"I login to Kim crm application as RO kim editor")]
        public void GivenILoginToKimCrmApplicationAsROKimEditor()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            string env = CommonFunctions.GetResourceValue("Env");
            //Navigate to the url based on env
            if (env.Equals("dev"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_dev_login");
            }
            else if (env.Equals("test"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_test_login");

            }
            else if (env.Equals("stage"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_stage_login");

            }
            else if (env.Equals("prod"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_prod_login");

            }
            Kim2HomePage kim2homePage = new Kim2HomePage(Driver);
            kim2homePage.SignIn_ROEditor();
            //CommonFunctions.GetUrl(Driver, "Kim2Url_dev");
        }
        [Given(@"I login to Kim crm application as RO kim approver")]
        public void GivenILoginToKimCrmApplicationAsROKimApprover()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            string env = CommonFunctions.GetResourceValue("Env");
            //Navigate to the url based on env
            if (env.Equals("dev"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_dev_login");
            }
            else if (env.Equals("test"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_test_login");

            }
            else if (env.Equals("stage"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_stage_login");

            }
            else if (env.Equals("prod"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_prod_login");

            }
            Kim2HomePage kim2homePage = new Kim2HomePage(Driver);
            kim2homePage.SignIn_ROApprover();
            //CommonFunctions.GetUrl(Driver, "Kim2Url_dev");
        }
        [Given(@"I login to Kim crm application as CO kim editor")]
        public void GivenILoginToKimCrmApplicationAsCOKimEditor()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            string env = CommonFunctions.GetResourceValue("Env");
            //Navigate to the url based on env
            if (env.Equals("dev"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_dev_login");
            }
            else if (env.Equals("test"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_test_login");

            }
            else if (env.Equals("stage"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_stage_login");

            }
            else if (env.Equals("prod"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_prod_login");

            }
            Kim2HomePage kim2homePage = new Kim2HomePage(Driver);
            kim2homePage.SignIn_COEditor();
            //CommonFunctions.GetUrl(Driver, "Kim2Url_dev");
        }
        [Given(@"I login to Kim crm application as CO kim approver")]
        public void GivenILoginToKimCrmApplicationAsCOKimApprover()
        {
            //Initialising the browser
            Driver = GetBrowser.InitBrowser(CommonFunctions.GetResourceValue("Browser"));
            string env = CommonFunctions.GetResourceValue("Env");
            //Navigate to the url based on env
            if (env.Equals("dev"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_dev_login");
            }
            else if (env.Equals("test"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_test_login");

            }
            else if (env.Equals("stage"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_stage_login");

            }
            else if (env.Equals("prod"))
            {
                CommonFunctions.GetUrl(Driver, "Kim2Url_prod_login");

            }
            Kim2HomePage kim2homePage = new Kim2HomePage(Driver);
            kim2homePage.SignIn_COApprover();
            //CommonFunctions.GetUrl(Driver, "Kim2Url_dev");
        }
        

        [Given(@"I wait for (.*) mins")]
        public void GivenIWaitForMins(int ms)
        {
            Thread.Sleep(ms);
        }



        [Then(@"I logout of Kim crm application")]
        public void ThenILogoutOfKimCrmApplication()
        {
            Kim2HomePage kim2homePage = new Kim2HomePage(Driver);
            kim2homePage.SignOut();
        }

        [When(@"Kim editor completes the task and change the status")]
        public void WhenKimEditorCompletesTheTaskAndChangeTheStatus(Table table)
        {
            Kim2ServicePage kim2ServicePage = new Kim2ServicePage(Driver);
            kim2ServicePage.FillForm_Acs1(table);
        }

        [When(@"Kim editor completes the task and change the status to CO approver request")]
        public void WhenKimEditorCompletesTheTaskAndChangeTheStatusToApprove()
        {
            Kim2ServicePage kim2ServicePage = new Kim2ServicePage(Driver);
            kim2ServicePage.FillForm_CSK4();
        }

        [When(@"Kim editor completes the task without Eym start date and change the status")]
        public void WhenKimEditorCompletesTheTaskWithoutEymStartDateAndChangeTheStatus(Table table)
        {
            Kim2ServicePage kim2ServicePage = new Kim2ServicePage(Driver);
            kim2ServicePage.FillForm_Acs1_WithoutEymStartDate(table);
        }

        [Then(@"Kim editor marks the (.*) complete")]
        public void ThenKimEditorMarksTheTaskComplete(string task)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.NavigateToTask();
            crmHomepage.TaskComplete(task);
        }
        [Then(@"Kim editor verifies the Request to remove a (.*) created")]
        public void ThenKimEditorVerifiesTheRequestToRemoveAServiceRejectedUpdateInProgressCreated(string task)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.NavigateToTask();
            crmHomepage.VerifyTask(task);
        }

        [Then(@"Kim editor marks the (.*) complete for enrolment")]
        public void ThenKimEditorMarksTheChildPossibleDuplicateRecordCompleteForEnrolment(string task)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.NavigateToTask_Child();
            crmHomepage.TaskComplete(task);
        }


        [When(@"Kim approver checks for data and changes the status to Approve")]
        public void WhenKimApproverChecksForDataAndChangesTheStatusToApprove()
        {
            Kim2ServicePage kim2ServicePage = new Kim2ServicePage(Driver);
            kim2ServicePage.Status_Approve();
        }


        [Then(@"Kim approver marks the (.*) complete")]
        public void ThenKimApproverMarksTheECService_EYMStatusHasChangedComplete(string task)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.NavigateToTask();
            crmHomepage.TaskComplete(task);
        }


        [When(@"I search for added service (.*)")]
        public void WhenISearchForAddedService(string addedService)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.SearchForAddedService(Driver, addedService);
        }
        [When(@"As a Kim editor I search for added service (.*)")]
        public void WhenAsAKimEditorISearchForAddedService(string addedService)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.SearchForAddedService_Kimeditorapprover(Driver, addedService);
        }
        [When(@"As a Kim approver I search for added service (.*)")]
        public void WhenAsAKimApproverISearchForAddedService(string addedService)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.SearchForAddedService_Kimeditorapprover(Driver, addedService);
        }



        [When(@"editor completes the pending tasks")]
        public void WhenEditorCompletesThePendingTasks(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.EnterData_NewService(table);

        }
        [When(@"approver approves the (.*) with (.*)")]
        public void WhenApproverApprovesTheWith(string addedService, string status)
        {

            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.ApproveService(status);
        }

        [Then(@"the service is ready for funding")]
        public void ThenTheServiceIsReadForFunding()
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.ServiceNameLink_Click();
            crmHomepage.Verify_ServiceStatus();
        }
        [Then(@"I verify the confirm (.*) of funding information")]
        public void ThenIVerifyTheConfirmStatusOfFundingInformation(string status)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.Verify_FundingStatus(status);
        }
        [Then(@"I verify the following precommitments and funding category")]
        public void ThenIVerifyTheFollowingPrecommitmentsAndFundingCategory(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.Verify_PreCommitments(table);
        }


        [When(@"I go to manage jobs to run the messages batch job")]
        public void WhenIGoToManageJobsToRunTheMessagesBatchJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            CommonFunctions.GetUrl(Driver, "ManageJobs_Url");
            mgHomepage.ManageJobsSignIn();           
        }

        [When(@"I run the messages batch job")]
        public void WhenIRunTheMessagesBatchJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged in
            try
            {
                mgHomepage.SearchJobName1();
            }
            catch
            {
                Console.WriteLine("User not logged in. It can be due to multiple reasons");
            }
        }

        [Then(@"I verify the (.*) is added successfully to the organisation in Kim sharepoint")]
        public void ThenIVerifyTheIsAddedSuccessfullyToTheOrganisationInKimSharepoint(string AddedNewService)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            // Check if the new added service exists and if exists click on it
            spHomePage.SearchService(AddedNewService);
        }

        [Then(@"I add teachers")]
        public void ThenIAddTeachers(Table table)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.AddNewTeacher();
            teacherPage.FillTeacherForm(table);
        }

        [Then(@"I add educators")]
        public void ThenIAddEducators(Table table)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.AddOtherEducators();
            educatorPage.FillEducatorForm(table);
        }
        [Then(@"I add sessional program for four year")]
        public void ThenIAddSessionalProgramForFourYear(Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_4yo();
            programPage.FillSessionalGroupForm(table);
        }
        [Then(@"I add sessional program for four year for new service")]
        public void ThenIAddSessionalProgramForFourYearForNewService(Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_4yo();
            programPage.FillSessionalGroupForm_NewService(table);
        }


        [Then(@"I add a sessional program for four and three year combined")]
        public void ThenIAddASessionalProgramForFourAndThreeYearCombined(Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_4yo3yo_Combined();
            programPage.FillSessionalGroupForm(table);
        }

        [Then(@"I add a sessional program for four and three year separate")]
        public void ThenIAddASessionalProgramForFourAndThreeYearSeparate(Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_4yo3yo_Separate();
            programPage.FillSessionalGroupForm(table);
        }
        [Then(@"I add a sessional program with rotational model for four year old and room (.*)")]
        public void ThenIAddASessionalProgramWithRotationalModelForFourYearOldRoomYellow(string room, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_Rotational_4yo();
            programPage.FillSessionalGroupForm_Rotational(table, room);
        }
        [Then(@"I add a sessional program with rotational model for four and three year old combined and room (.*)")]
        public void ThenIAddASessionalProgramWithRotationalModelForFourAndThreeYearOldCombinedAndRoomRoomYellow(string room, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_Rotational_4yo3yo_Combined();
            programPage.FillSessionalGroupForm_Rotational(table, room);
        }

        [Then(@"I add a sessional program with rotational model for four year and three year old separate and room (.*)")]
        public void ThenIAddASessionalProgramWithRotationalModelForFourYearAndThreeYearOldSeparateAndRoomRoomYellow(string room, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Sessional_Rotational_4yo3yo_Separate();
            programPage.FillSessionalGroupForm_Rotational(table, room);
        }
        [Then(@"I add integrated program with three year old program and number of (.*)")]
        public void ThenIAddIntegratedProgramWithThreeYearOldProgramAndNumberOf(string weeks, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Integrated_With3yo(weeks);
            programPage.FillIntegratedGroupForm(table);
        }

        [Then(@"I add integrated program without three year old program and number of (.*)")]
        public void ThenIAddIntegratedProgramWithoutThreeYearOldProgramAndNumberOf(string weeks, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_Integrated_Without3yo(weeks);
            programPage.FillIntegratedGroupForm(table);
        }



        [Then(@"I add programs when programs are set to sessional")]
        public void ThenIAddProgramsWhenProgramsAreSetToSessionalNoRotation(Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_ExistingService();
            programPage.FillSessionalGroupForm(table);
        }


        [Then(@"I add enrolments for YBSK")]
        public void ThenIAddEnrolmentsForYBSK(Table table)
        {
            EnrolmentsPage enrolmentPage = new EnrolmentsPage(Driver);
            enrolmentPage.AddNewEnrolments();
            enrolmentPage.FillNewEnrolmentForm(table);
        }
        [Then(@"I open an existing teacher to apply for parentalleave  with (.*)")]
        public void ThenIOpenAnExistingTeacherToApplyForParentalleaveWith(string teacherName)
        {
            LeaveApplicationPage leaveApplicationPage = new LeaveApplicationPage(Driver);
            leaveApplicationPage.AddNewParentalLeaveApplication(teacherName);
        }
        [Then(@"I open an existing teacher to apply for Travel Allowance with (.*)")]
        public void ThenIOpenAnExistingTeacherToApplyForTravelAllowanceWithFstNLstN(string teacherName)
        {
            LeaveApplicationPage leaveApplicationPage = new LeaveApplicationPage(Driver);
            leaveApplicationPage.Add_TravelAllowance_Application(teacherName);
        }
        [Then(@"I fill teachers application for travel leave")]
        public void ThenIFillTeachersApplicationForTravelLeave(Table table)
        {
            LeaveApplicationPage leaveApplicationPage = new LeaveApplicationPage(Driver);
            leaveApplicationPage.Fill_TravelForm(table);
        }

        [Then(@"I fill teachers application for parental leave")]
        public void ThenIFillTeachersApplicationForParentalLeave(Table table)
        {
            LeaveApplicationPage leaveApplicationPage = new LeaveApplicationPage(Driver);
            leaveApplicationPage.FillNewParentalLeaveForm(table);
        }

        [Then(@"I go to manage jobs url and run the appropriate job")]
        public void ThenIGoToManageJobsUrlAndRunTheAppropriateJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            CommonFunctions.GetUrl(Driver, "ManageJobs_Url");
            mgHomepage.ManageJobsSignIn();
            mgHomepage.SearchJobName();
            
        }



        [Then(@"I verify the annual confirmation in Kim sharepoint")]
        public void ThenIVerifyTheAnnualConfirmationInKimSharepoint()
        {
            Kim1Homepage homePage = new Kim1Homepage(Driver);
            homePage.GetUrl();
            homePage.LoggedIn();
            homePage.ServiceProviderClick();
            ServiceProviderListPage spListPage = new ServiceProviderListPage(Driver);
            spListPage.SearchAndClick();
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.SearchService(AddedNewService);
            spHomePage.AnnualConfirmation();

        }
        [Then(@"I logout of manage jobs")]
        public void ThenILogoutOfManageJobs()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            mgHomepage.LogOut();
            //Thread.Sleep(150000);
        }
        [When(@"I logout of Kim sharepoint")]
        public void WhenILogoutOfKimSharepoint()
        {
            // Kim1Homepage homePage = new Kim1Homepage(Driver);

        }

        [Then(@"I close the browser successfully")]
        public void ThenICloseTheBrowserSuccessfully()
        {
            GetBrowser.Cleanup();
        }
        [Then(@"I verify that the services is added successfully")]
        public void ThenIVerifyThatTheServicesIsAddedSuccessfully()
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.CloseButton.Click();
            spHomePage.AddedSuccess();
        }
        [Then(@"I delete an existing teacher with (.*)")]
        public void ThenIDeleteAnExistingTeacherWith(string teacherName)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.DeleteTeacher(teacherName);

        }

        [Then(@"I enter final date and (.*) for leaving and confirm removal")]
        public void ThenIEnterAndResignedForLeavingAndConfirmRemoval(string reason)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.ConfirmRemove_Teacher(reason);
        }

        [Then(@"I verify that teacher with (.*) is deleted successfully")]
        public void ThenIVerifyThatTeacherIsDeletedSuccessfully(string teacher)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.VerifyDelete_Teacher(teacher);
        }

        [Then(@"I open an existing teacher with (.*)")]
        public void ThenIOpenAnExistingTeacherWith(string teacherName)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.EditTeacher(teacherName);
        }


        [Then(@"I edit (.*) of teacher")]
        public void ThenIEdit(string commenceDate)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.EditCommenceDate(commenceDate);
        }
        [Then(@"I edit number of (.*)")]
        public void ThenIEditNumberOf(string numGroups)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.EditGroupNum(numGroups);
        }


        [Then(@"I open an existing educator with (.*)")]
        public void ThenIOpenAnExistingEducatorWith(string name)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);

            educatorPage.EditEducator(name);

        }
        [Then(@"I edit (.*) and number of employment (.*) of an educator")]
        public void ThenIEditAndNumberOfEmploymentOfAnEducator(string dob, string hoursEmployment)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.EditDob(dob);
            educatorPage.EditHoursEmployment(hoursEmployment);
        }


        [Then(@"I edit (.*) of educator")]
        public void ThenIEditOfEducator(string dob)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.EditDob(dob);
        }

        [Then(@"I edit number of employment (.*)")]
        public void ThenIEditNumberOfEmployment(string hoursEmployment)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.EditHoursEmployment(hoursEmployment);
        }

        [Then(@"I delete an existing educator (.*) with (.*)")]
        public void ThenIDeleteAnExistingEducatorWith(string name, string reason)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.DeleteEducator(name);
            educatorPage.ConfirmRemove_Educator(reason);
        }

        [Then(@"I verify that educator (.*) is deleted successfully")]
        public void ThenIVerifyThatEducatorIsDeletedSuccessfully(string name)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.VerifyDeleteEducator(name);
        }

        [Then(@"I open an existing enrolment with (.*)")]
        public void ThenIOpenAnExistingEnrolmentWith(string slk)
        {
            EnrolmentsPage enrolmentsPage = new EnrolmentsPage(Driver);
            enrolmentsPage.EditEnrolment(slk);
        }

        [Then(@"I edit (.*) of the enrolled student")]
        public void ThenIEditOfTheEnrolledStudent(string date)
        {
            EnrolmentsPage enrolmentsPage = new EnrolmentsPage(Driver);
            enrolmentsPage.EditCommenceDate_Enrolment(date);
        }

        [Then(@"I delete an existing enrolment with (.*) and (.*)")]
        public void ThenIDeleteAnExistingEnrolmentWith(string slk, string reason)
        {
            EnrolmentsPage enrolmentsPage = new EnrolmentsPage(Driver);
            enrolmentsPage.DeleteEnrolment(slk);
            enrolmentsPage.ConfirmRemove_Enrolment(reason);
        }

        [Then(@"I confirm removal of enrolment (.*)")]
        public void ThenIConfirmRemovalOfEnrolment(string slk)
        {
            EnrolmentsPage enrolmentsPage = new EnrolmentsPage(Driver);
            enrolmentsPage.Confirm_DeleteEnrolment(slk);
        }

        [Then(@"I bulk upload the enrolments")]
        public void ThenIBulkUploadTheEnrolments()
        {
            EnrolmentsPage enrolmentsPage = new EnrolmentsPage(Driver);
            enrolmentsPage.UploadEnrolmentFile();
        }

        [Then(@"I edit a (.*) with (.*) and fill in details and submit")]
        public void ThenIEditAPondsKidsWithAndFillInDetailsAndSubmit(string service, string telephone)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            winHandleBefore = spHomePage.EditService(service);
            spHomePage.EditPhone(telephone);
        }


        [Then(@"I verify that the service (.*) is edited successfully")]
        public void ThenIVerifyThatTheServiceIsEditedSuccessfully(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);

            spHomePage.Verify_EditService(service, winHandleBefore);
        }

        [Then(@"I delete a (.*)")]
        public void ThenIDeleteAService(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.DeleteService(service);
        }
        [Then(@"I verify that the service (.*) is deleted successfully")]
        public void ThenIVerifyThatServiceIsDeletedSuccessfully(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.ConfirmDeleteService(service);
        }
        [Then(@"I verify that the service (.*) is deleted from kim sharepoint")]
        public void ThenIVerifyThatTheServiceIsDeletedFromKimSharepoint(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.ConfirmDeleteService_Assert(service);
        }


        [Then(@"I verify that the service (.*) is deleted successfully with past date")]
        public void ThenIVerifyThatTheServiceMooneeKidsKindergartenIsDeletedSuccessfullyWithPastDate(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.ConfirmDeleteService_PastDate(service);
        }


        [Then(@"I verify that the service(.*) can be searched")]
        public void ThenIVerifyCanBeSearched(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.FindService_EditDelete(service);
        }


        [Then(@"I confirm removal of group (.*)")]
        public void ThenIConfirmRemovalOfGroup(string groupName)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Confirm_DeleteProgram();
            programPage.Verify_RemoveProgram(groupName);
        }
        [Then(@"I click on submit for processing")]
        public void ThenIClickOnSubmitForProcessing()
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.SubmitProcessing();
        }

        [Then(@"I delete an existing program with name (.*)")]
        public void ThenIDeleteAnExistingProgramWithName(string groupName)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.DeleteProgram(groupName);
        }
        [Then(@"I add ESK teachers")]
        public void ThenIAddESKTeachers(Table table)
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.AddNewESKTeacher();
            ESKTeacherPage.FillESKTeacherForm(table);
        }
        [Then(@"I open an existing ESKteacher with (.*)")]
        public void ThenIOpenAnExistingESKteacherWith(string ESKteacherName)
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.EditESKTeacher(ESKteacherName);
        }

        [Then(@"I edit (.*) of ESKteacher")]
        public void ThenIEditD(string commenceDate)
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.EditCommenceDate(commenceDate);
        }

        [Then(@"I edit the (.*)")]
        public void ThenIEditThe(string yearawarded)
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.EditYearAwarded(yearawarded);
        }

        [Then(@"I delete an existing ESKteacher with(.*)")]
        public void ThenIDeleteAnExistingESKteacherWith(string ESKteacherName)
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.DeleteESKTeacher(ESKteacherName);
        }

        [Then(@"I enter lastdate and (.*) for leaving and confirm removal")]
        public void ThenIEnterLastdateAndResignedForLeavingAndConfirmRemoval(string reason)
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.ConfirmRemove_ESKTeacher(reason);
        }
        [Then(@"I verify that ESKteacher with (.*) is deleted successfully")]
        public void ThenIVerifyThatESKteacherIsDeletedSuccessfully(string ESKteacherName)
        {

            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.VerifyDelete_ESKTeacher(ESKteacherName);
        }

        [Then(@"I add enrolments for ESK")]
        public void ThenIAddEnrolmentsForESK(Table table)
        {
            ESKEnrolmentsPage ESKEnrolmentsPage = new ESKEnrolmentsPage(Driver);
            ESKEnrolmentsPage.AddNewESKEnrolments();
            ESKEnrolmentsPage.FillNewESKEnrolmentForm(table);
        }


        [Then(@"I open an existing eskenrolment with (.*)")]
        public void ThenIOpenAnExistingEskenrolmentWith(string SLK)
        {
            ESKEnrolmentsPage ESKEnrolmentsPage = new ESKEnrolmentsPage(Driver);
            ESKEnrolmentsPage.EditESKEnrolment(SLK);

        }


        [Then(@"I edit (.*) of the eskenrolled student")]
        public void ThenIEditOfTheEskenrolledStudent(string Street)
        {
            ESKEnrolmentsPage eskEnrolmentsPage = new ESKEnrolmentsPage(Driver);
            eskEnrolmentsPage.EditStreet(Street);
        }

        [Then(@"I add Programs for ESK")]
        public void ThenIAddProgramsForESK(Table table)
        {
            ESKProgramPage ESKProgramPage = new ESKProgramPage(Driver);
            ESKProgramPage.AddNewESKProgram();
            ESKProgramPage.FillESKProgramForm(table);
        }
        [Then(@"I export teachers data")]
        public void ThenIExportTeachersData()
        {
            ExportToExcelPage ExportToExcelPage = new ExportToExcelPage(Driver);
            ExportToExcelPage.ExportToExcel();
        }
        [Then(@"I open an existing program with name (.*)")]
        public void ThenIOpenAnExistingProgramWithNameBlueGroup(string groupName)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.EditProgram(groupName);

        }
        [Then(@"I verify that program is successfully edited (.*)")]
        public void ThenIVerifyThatProgramIsSuccessfullyEditedHappyKids(string groupName)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Verify_EditProgram(groupName);
        }

        [Then(@"I complete teachers tab")]
        public void ThenICompleteTeachersTab(Table table)
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.CompleteTeacherData(table);
        }
        [Then(@"I complete educator tab (.*)")]
        public void ThenICompleteEducatorTab(String hours)
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.CompleteEducatorData(hours);

        }
        [Then(@"I complete program tab")]
        public void ThenICompleteProgramTab()
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.CompleteProgramData();
        }
        [Then(@"I click on annual confirmation tab")]
        public void ThenIClickOnAnnualConfirmationTab()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.AnnualConfirm();
        }
        [Then(@"I verify the available fields in annual confirmation tab")]
        public void ThenIVerifyTheAvailableFieldsInAnnualConfirmationTab()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.FieldsAnnualConfirmationTab();
        }
        [Then(@"I verify the values in annual confirmation tab")]
        public void ThenIVerifyTheValuesInAnnualConfirmationTab()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ValuesAnnualConfirmationTab();
        }
        [Then(@"I check the status for Teachers, Educators, Program, and Enrolments tabs all COMPLETE")]
        public void ThenICheckTheStatusForTeachersEducatorsProgramAndEnrolmentsTabsAllCOMPLETE()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.CheckTEPEStatus();
        }

        [Then(@"I verify the status for Annual Confirmation should be COMPLETE")]
        public void ThenIVerifyTheStatusForAnnualConfirmationShouldBeCOMPLETE()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.AnnualConfirmStatus();
        }

        [Then(@"I click confirm and submit the data")]
        public void ThenIClickConfirmAndSubmitTheData(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm(table);
        }
        [Then(@"I click confirm and submit the data for sessional program")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalProgram(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_Sessional(table);
        }
        [Then(@"I click confirm and submit the data for sessional program with not checked checkboxes")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalProgramWithNotCheckedCheckboxes(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_Sessional_NoCheckbox(table);
        }
        [When(@"Kim RO editor completes the task with (.*)")]
        public void WhenKimROEditorCompletesTheAnnualConfirmationNoQualifiedTeacherTask(string note)
        {
            Kim2ServicePage kimServicePage = new Kim2ServicePage(Driver);
            kimServicePage.AddNote(note);
        }

        [Then(@"I click confirm and submit the data for sessional rotational program")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalRotationalProgram(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_Sessional(table);
        }

        [Then(@"I verify the alert")]
        public void ThenIVerifyTheAlert()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.AlertforNOTComplete();
        }

        [Then(@"I verify and close the alert")]
        public void ThenIVerifyAndCloseTheAlert()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.AlertforNOTComplete();
        }
        [Then(@"I click confirm data")]
        public void ThenIClickConfirmData(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm(table);
        }

        [When(@"I click on reports")]
        public void WhenIClickOnReports()
        {
            Kim1Homepage kim1Homepage = new Kim1Homepage(Driver);
            kim1Homepage.ClickReports();
        }

        [Then(@"I download Annual Confirmation report")]
        public void ThenIDownloadAnnualConfirmationReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadACRReport();
        }

        [Then(@"I download ESK Survey report")]
        public void ThenIDownloadESKSurveyReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadESKSurveyReport();
        }

        [Then(@"I download Geo Code Summary for Enrolments report")]
        public void ThenIDownloadGeoCodeSummaryForEnrolmentsReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadGeoCodeSummaryReport();
        }

        [Then(@"I download Non English Speaking Background report")]
        public void ThenIDownloadNonEnglishSpeakingBackgroundReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadNonEnglishSpeakingBackgroundReport();
        }

        [Then(@"I download Rotational groups extract for ratio funding report")]
        public void ThenIDownloadRotationalGroupsExtractForRatioFundingReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadRotationalGroupReport();
        }

        [Then(@"I download Sessional groups report for ratio funding")]
        public void ThenIDownloadSessionalGroupsReportForRatioFunding()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadSessionalGroupReport();
        }

        [Then(@"I download SPG Overview Report")]
        public void ThenIDownloadSPGOverviewReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadSPGOverviewReport();
        }
        [Then(@"I add KFS enrolment")]
        public void ThenIAddKFSEnrolment(Table table)
        {
            EnrolmentsPage enrolmentPage = new EnrolmentsPage(Driver);
            enrolmentPage.AddNewEnrolments();
            enrolmentPage.FillNewEnrolmentForm_KFS(table);
        }

        [Then(@"I add ESK EXTN enrolment")]
        public void ThenIAddESKEXTNEnrolment(Table table)
        {
            EnrolmentsPage enrolmentPage = new EnrolmentsPage(Driver);
            enrolmentPage.AddNewEnrolments();
            enrolmentPage.FillNewEnrolmentForm_EskExtn_CP(table);
        }

        [When(@"I run the enrolment confirmation job")]
        public void WhenIRunTheEnrolmentConfirmationJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged 
            mgHomepage.SearchEnrolmentConfJob();

        }
        [When(@"I run the parental leave job")]
        public void WhenIRunTheParentalLeaveJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged 
            mgHomepage.SearchParentalJob();
        }
        [When(@"I run the travel leave job")]
        public void WhenIRunTheTravelLeaveJob()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            //Run the job if user is successfully logged 
            mgHomepage.SearchTravelJob();
        }


        [When(@"I go back to job list")]
        public void WhenIGoBackToJobList()
        {
            ManageJobsHomePage mgHomepage = new ManageJobsHomePage(Driver);
            mgHomepage.Navigate_JobList();
        }

        [When(@"I click on Service Provider report")]
        public void WhenIClickOnServiceProviderReport()
        {
            ServiceProviderHomePage serviceProviderHomePage = new ServiceProviderHomePage(Driver);
            serviceProviderHomePage.Reports();
            serviceProviderHomePage.ServiceProviderReports();
        }

        [Then(@"I download Service Provider Funding Commitment Report")]
        public void ThenIDownloadServiceProviderFundingCommitmentReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadServiceProviderFundingCommitmentReport();
        }

        [When(@"I click on Service report")]
        public void WhenIClickOnServiceReport()
        {
            ServiceProviderHomePage serviceProviderHomePage = new ServiceProviderHomePage(Driver);
            serviceProviderHomePage.Reports();
            serviceProviderHomePage.ServiceReports();
        }

        [Then(@"I download Forms By Service Location Report")]
        public void ThenIDownloadFormsByServiceLocationReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadFormsByServiceLocationReport();
        }

        [Then(@"I download Service Summary Report")]
        public void ThenIDownloadServiceSummaryReport()
        {
            ReportsPage reportsPage = new ReportsPage(Driver);
            reportsPage.DownloadServiceSummaryReport();
        }

        [Then(@"I click confirm and submit the data for sessional program - Separate")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalProgram_Separate(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_SessionalSeparateCombined(table);
        }

        [Then(@"I click confirm and submit the data for sessional program Combined")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalProgramCombined(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_SessionalSeparateCombined(table);
        }

        [Then(@"I click confirm and submit the data for sessional rotational program - Separate")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalRotationalProgram_Separate(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_SessionalSeparateCombined(table);
        }

        [Then(@"I click confirm and submit the data for sessional rotationl program Combined")]
        public void ThenIClickConfirmAndSubmitTheDataForSessionalRotationlProgramCombined(Table table)
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ConfirmData();
            annualConfirmationPage.FillAnnualConfirmationForm_SessionalSeparateCombined(table);
        }
        [Then(@"I export teachers and enrolment data")]
        public void ThenIExportTeachersandEnrolmentData()
        {
            ExportToExcelPage ExportToExcelPage = new ExportToExcelPage(Driver);
            ExportToExcelPage.ExportToExcel();
        }



        [Then(@"I export other educators data")]
        public void ThenIExportOtherEducatorsData()
        {
            ExportToExcelPage ExportToExcelPage = new ExportToExcelPage(Driver);
            ExportToExcelPage.YBSK_ExportToExcel_Educators();
        }
        [Then(@"I export programs data")]
        public void ThenIExportProgramsData()
        {
            ExportToExcelPage ExportToExcelPage = new ExportToExcelPage(Driver);
            ExportToExcelPage.YBSK_ExportToExcel_Programs();
        }

        [Then(@"I export enrolments data")]
        public void ThenIExportEnrolmentsData()
        {
            ExportToExcelPage ExportToExcelPage = new ExportToExcelPage(Driver);
            ExportToExcelPage.YBSK_ExportToExcel_Enrolments();
        }

        [Then(@"I add both integrated and sessional programs with rotational program and four year old program and number of (.*)")]
        public void ThenIAddBothIntegratedAndSessionalProgramsWithRotationalProgramAndFourYearOldProgramAndNumberOf(string weeks, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_SessionalIntegrated_RotationalYes_4yo(weeks);
            programPage.FillIntegratedGroupForm(table);
        }

        [Then(@"I add both integrated and sessional programs without rotational program and four year old program and number of (.*)")]
        public void ThenIAddBothIntegratedAndSessionalProgramsWithoutRotationalProgramAndFourYearOldProgramAndNumberOf(string weeks, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.Programs_SessionalIntegrated_RotationalNo_4yo(weeks);
            programPage.FillIntegratedGroupForm(table);
        }

        [Then(@"I add a sessional program also and room (.*)")]
        public void ThenIAddASessionalProgramAlso(string room, Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.FillSessionalGroupForm_Rotational(table, room);
        }
        [Then(@"I add a sessional program also")]
        public void ThenIAddASessionalProgramAlso(Table table)
        {
            ProgramsPage programPage = new ProgramsPage(Driver);
            programPage.FillSessionalGroupForm(table);
        }
        [Then(@"I add click show inactive and make inactive ESK teacher	to active")]
        public void ThenIAddClickShowInactiveAndMakeInactiveESKTeacherToActive()
        {
            ESKTeacherPage ESKTeacherPage = new ESKTeacherPage(Driver);
            ESKTeacherPage.MakeInactiveESKTeacherActive();
        }
        [Then(@"I add click show inactive and make inactive teacher\tto active")]
        public void ThenIAddClickShowInactiveAndMakeInactiveTeacherToActive()
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            teacherPage.MakeInactiveTeacherActive();
        }
        [Then(@"I add click show inactive and make inactive educator to active")]
        public void ThenIAddClickShowInactiveAndMakeInactiveEducatorToActive()
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            educatorPage.MakeInactiveEducatorActive();
        }
        [Then(@"I verify the status for Teachers, Educators, Program, and Enrolments tabs all Read Only")]
        public void ThenIVerifyTheStatusForTeachersEducatorsProgramAndEnrolmentsTabsAllReadOnly()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.CheckTEPEStatus_AfterAnnualConfirmationSubmit();
        }
        [Then(@"I recommence a (.*)")]
        public void ThenIRecommenceAAltonaWestKindergarten(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.RecommenceService(service);
        }
        [Then(@"I verify (.*) recommenced")]
        public void ThenIVerifyAltonaWestKindergartenRecommenced(string service)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.Verify_Service_Recommenced(service);
        }
        [Then(@"I fill recommence information")]
        public void ThenIFillRecommenceInformation(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.FillRecommenceForm(table);
        }
        [Then(@"I fill recommence information from EYM to not EYM")]
        public void ThenIFillRecommenceInformationFromEYMToNotEYM(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.FillRecommenceFormFromEYMtoNotEYM(table);
        }

        [Then(@"I fill recommence information from EYM to EYM")]
        public void ThenIFillRecommenceInformationFromEYMToEYM(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.FillRecommenceFormFromEYMtoEYM(table);
        }

        [Then(@"I fill recommence information from EYM to EYM SUBMITTED")]
        public void ThenIFillRecommenceInformationFromEYMToEYMSUBMITTED(Table table)
        {
            ServiceProviderHomePage spHomePage = new ServiceProviderHomePage(Driver);
            spHomePage.FillRecommenceFormFromEYMtoEYMSUBMIITED(table);
        }
        [Then(@"I verify and close the adjustment alert")]
        public void ThenIVerifyAndCloseTheAdjustmentAlert()
        {
            AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
            adjustmentsPage.AdjustmentsAlert();
        }

        [Then(@"I verify the status for Teachers, Educators, Program, and Enrolments tabs are all Read Only and Annual Confirmation tab is Submitted")]
        public void ThenIVerifyTheStatusForTeachersEducatorsProgramAndEnrolmentsTabsAreAllReadOnlyAndAnnualConfirmationTabIsSubmitted()
        {
            AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
            adjustmentsPage.CheckTEPEAStatus();
        }
        [Then(@"I verify the status for Teachers, Educators, Program, Enrolments, and Annual Confirmation tabs are all COMPLETE")]
        public void ThenIVerifyTheStatusForTeachersEducatorsProgramEnrolmentsAndAnnualConfirmationTabsAreAllCOMPLETE()
        {
            AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
            adjustmentsPage.CheckTEPEAStatusAfterApprove();
        }
        [Then(@"I verify the adjustment tab status should be available")]
        public void ThenIVerifyTheAdjustmentTabStatusShouldBeAvailable()
        {
            AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
            adjustmentsPage.AdjustmentsTabStatus();
        }
        [Then(@"I verify the available fields in Adjustments tab")]
        public void ThenIVerifyTheAvailableFieldsInAdjustmentsTab()
        {
            AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
            adjustmentsPage.AdjustmentsTabAvailableFields();

        }
        [Then(@"I verify the correctness of values in Adjustments tab")]
        public void ThenIVerifyTheCorrectnessOfValuesInAdjustmentsTab()
        {
            AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
            adjustmentsPage.VerifyValuesAdjustmentsTab();
        }
        [Then(@"I click View Data button")]
        public void ThenIClickViewDataButton()
        {
            AnnualConfirmationPage annualConfirmationPage = new AnnualConfirmationPage(Driver);
            annualConfirmationPage.ViewData();
            annualConfirmationPage.ViewAnnualConfirmationForm();
        }
        [Then(@"I click on adjustments tab")]
        public void ThenIClickOnAdjustmentsTab()
        {
            {
                AdjustmentsPage adjustmentsPage = new AdjustmentsPage(Driver);
                adjustmentsPage.Adjustments();
            }
        }
        [Then(@"I approve the following precommitments and funding category")]
        public void ThenIApproveTheFollowingPrecommitmentsAndFundingCategory(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.Approve_PreCommitments(table);
        }
        [Then(@"I approve the following precommitments and funding category with task (.*) and (.*)")]
        public void ThenIApproveTheFollowingPrecommitmentsAndFundingCategoryWithTask(string task, string note, Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.Approve_PreCommitments_withTasks(table, task, note);
        }

        [Then(@"I verify the following confirmed children/enrolments")]
        public void ThenIVerifyTheFollowingConfirmedChildrenEnrolments(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.Verify_ConfirmedEnrolment_Children(table);
        }
        [When(@"Kim editor navigate to duplicate enrolment and go to activities")]
        public void WhenKimEditorNavigateToDuplicateEnrolmentAndGoToActivities(Table table)
        {
            Kim2ServicePage kimServicePage = new Kim2ServicePage(Driver);
            kimServicePage.NavigateEnrolment(table);
        }
        [Then(@"I verify the \# of teacher and co-educator")]
        public void ThenIVerifyTheOfTeacherAndCo_Educator()
        {

            Kim2ServicePage kimServicePage = new Kim2ServicePage(Driver);
            kimServicePage.Verify_Workforce(eRNoofTeacher, eRNoofEducator);
        }
        [Then(@"I check the number of teacher in Teachers tab")]
        public void ThenICheckTheNumberOfTeacherInTeachersTab()
        {
            TeacherPage teacherPage = new TeacherPage(Driver);
            eRNoofTeacher = teacherPage.NoofTeacher();
        }

        [Then(@"I check the number of educator in Other Educators tab")]
        public void ThenICheckTheNumberOfEducatorInOtherEducatorsTab()
        {
            EducatorPage educatorPage = new EducatorPage(Driver);
            eRNoofEducator = educatorPage.NoofEducator();
        }
        [Then(@"I check if all ESK tabs are complete")]
        public void ThenICheckIfAllESKTabsAreComplete()
        {
            ESKTeacherPage eskTeacherPage = new ESKTeacherPage(Driver);
            eskTeacherPage.GoToEsk_Link();
            eskTeacherPage.EskTeacherStatus();
            ESKProgramPage eskProgram = new ESKProgramPage(Driver);
            eskProgram.EskProgramStatus();
            ESKEnrolmentsPage eskEnrolmentsPage = new ESKEnrolmentsPage(Driver);
            eskEnrolmentsPage.EskEnrolmentStatus();
        }
        [Then(@"I open an existing teacher to check if can apply for parentalleave  with (.*)")]
        public void ThenIOpenAnExistingTeacherToCheckIfCanApplyForParentalleaveWithTeacherWilliam(string teacherName)
        {
            LeaveApplicationPage ParentalLeaveApplicationPage = new LeaveApplicationPage(Driver);
            ParentalLeaveApplicationPage.CheckApplicationbutton(teacherName);
        }
        [Then(@"I check the Workforce Parental Leave and request for manager approval")]
        public void ThenICheckTheWorkforceParentalLeaveAndRequestForManagerApproval(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.WorkforceParentalLeave(table);
        }

        [Then(@"I approve parental leave")]
        public void ThenIApproveParentalLeave(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.WorkforceParentalLeaveApproval(table);
        }
        [Then(@"I add enrolments for YBSK Integrated")]
        public void ThenIAddEnrolmentsForYBSKIntegrated(Table table)
        {
            EnrolmentsPage enrolmentPage = new EnrolmentsPage(Driver);
            enrolmentPage.AddNewEnrolments();
            enrolmentPage.FillNewEnrolmentForm_Integrated(table);
        }
        
        [Then(@"Kim editor view the (.*)")]
        public void ThenKimEditorViewTheTask(string task)
        {
            Kim2HomePage crmHomepage = new Kim2HomePage(Driver);
            crmHomepage.NavigateToTask();
            crmHomepage.VerifyTask(task);
        }

        [When(@"I check the Workforce Leave Approval if Travel Allowance application was added")]
        public void WhenICheckTheWorkforceLeaveApprovalIfTravelAllowanceApplicationWasAdded(Table table)
        {
            Kim2ServicePage servicePage = new Kim2ServicePage(Driver);
            servicePage.WorkforceTravelAllowance_ServiceMissingInformation(table);
        }

    }
}