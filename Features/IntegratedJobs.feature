﻿Feature: IntegratedJobs
	Automate the jobs running from Kim sharepoint to crm and vice versa

	#To import any Service information submitted in KIM SharePoint to Kim crm
    @functional
Scenario: Run KIM sharepoint to crm service
	Given I go to manage job url
	When I run the services batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully

	# To update the CRM Status of a record in KIM SharePoint
	@functional
Scenario: Run KIM crm message to KIM sharepoint
	Given I go to manage job url
	When I run the messages batch job
	Then the service is successfully run
	And I logout of manage jobs
	And I close the browser successfully
