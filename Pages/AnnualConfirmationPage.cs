﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;


namespace KIM_Automation.Pages
{
    class AnnualConfirmationPage : BaseApplicationPage
    {
        public AnnualConfirmationPage(IWebDriver driver) : base(driver)
        { }

        //Annual Confirmation Elements
        //public IWebElement AnnualConfirmationTabElement => Driver.FindElement(By.CssSelector("#profileTabsNav > li:nth-child(5) > a"));
        public By AnnualConfirmationTabElement => By.CssSelector("#profileTabsNav > li:nth-child(5) > a");
        public By AnnualConfirmationTabStatusElement => By.XPath("//span[@class='status confirm-tab-status']");
        public By ProgramColumnHeaderElement => By.XPath("//a[contains(text(),'Program')]");
        public By ProgramValueElement => By.XPath("//tbody[@id='confirmTable']//td");
        public By TeachersColumnHeaderElement => By.XPath("//a[contains(text(),'Teachers')]");
        public By TeachersValueElement => By.XPath("//div[@id='tab5']//td[2]");
        public By OtherEducatorsColumnHeaderElement => By.XPath("//a[contains(text(),'Other Educators')]");
        public By OtherEducatorsValueElement => By.XPath("//div[@id='tab5']//td[3]");
        public By EnrolmentsColumnHeaderElement => By.XPath("//div[@id='tab5']//th[4]//a[1]");
        public By EnrolmentsValueElement => By.XPath("//div[@id='tab5']//td[4]");
        public By KFSEnrolmentsColumnHeaderElement => By.XPath("//a[contains(text(),'KFS Enrolments')]");
        public By KFSEnrolmentsValueElement => By.XPath("//div[@id='tab5']//td[5]");
        //public By OptionsColumnHeaderElement => By.XPath("//[contains(text(),'Options')]");
        public By OptionsColumnHeaderElement => By.XPath("//table[@id='DataTables_Table_1']//th[contains(@class,'sorting_asc')][contains(text(),'Options')]");
        public By ConfirmDataButtonElement => By.XPath("//tbody[@id='confirmTable']//button[contains(text(),'Confirm Data')]");
        public By ViewDataButtonElement => By.XPath("//tbody[@id='confirmTable']//button[contains(text(),'View Data')]");
        //public IWebElement ConfirmDataElement => Driver.FindElement(By.XPath("//button[@class='btn blue edit']"));
        //public IWebElement ConfirmDataElement => Driver.FindElement(By.XPath("//button[contains(text(),'Confirm Data')]"));
        //public IWebElement ConfirmDataElement => Driver.FindElement(By.CssSelector("button.btn.blue.edit:contains('Confirm Data')"));
        //public IWebElement ConfirmDataElement => Driver.FindElement(By.CssSelector("button:contains('Confirm Data')"));
        //public IWebElement TableConfirmDataElement => Driver.FindElement(By.CssSelector("table[id='DataTables_Table_1']"));
        //public IWebElement ConfirmDataElement => TableConfirmDataElement.FindElement(By.CssSelector("button:contains('Confirm Data')"));
        public By AnnualConfirmFormElement => By.XPath("//form[@id='annualConfirmForm']");
        public By CentralEnrolmentYesOptionElement => By.CssSelector("#centralEnrolYes");
        public By CentralEnrolmentNoOptionElement => By.CssSelector("#centralEnrolNo");
        public By TLDSOneElement => By.CssSelector("#cnTransitionOne");
        public By TLDSTwoElement => By.CssSelector("#cnTransitionTwo");
        public By ThreeYrOldEnrolmentsChildrenElement => By.CssSelector("#cnTotal3yrEnrolments");
        public By ThreeYrOldEnrolmentsHoursPerWeekElement => By.CssSelector("#cnTotal3yrHoursPerWeek");
        public By FeeSubsidyYesOptionElement => By.CssSelector("#cnFeeSubsidyYes");
        public By FeeSubsidyNoOptionElement => By.CssSelector("#cnFeeSubsidyNo");
        public By FeeSubsidySurplusYesOptionElement => By.CssSelector("#cnSurplusYes");
        public By FeeSubsidySurplusNoOptionElement => By.CssSelector("#cnSurplusNo");
        public By AnnualConfirmationNextButtonElement => By.XPath("//form[@id='annualConfirmForm']//button[@name='btnNext'][contains(text(),'Next')]");
        public By AnnualConfirmationPreviousButtonElement => By.XPath("//form[@id='annualConfirmForm']//button[@name='btnPrev'][contains(text(),'Previous')]");
        public By AnnualConfirmationExitButtonElement => By.XPath("//form[@id='annualConfirmForm']//button[@name='btnExit'][contains(text(),'Exit')]");
        public By TotalOtherEarlyElement => By.CssSelector("#cnTotalOtherTeachers");
        public By MastersdegElement => By.CssSelector("#cnMasters");
        public By GraduatedegElement => By.CssSelector("#cnGradDip");
        public By DualBachelordegElement => By.CssSelector("#cnDual");
        public By BachelordegElement => By.CssSelector("#cnBachelor");
        public By PathwaydegElement => By.CssSelector("#cnPathway");
        public By DiplomadegElement => By.CssSelector("#cnDiploma");
        public By CertNameElement => By.CssSelector("#txtCertName");
        public By CertPositionElement => By.CssSelector("#txtCertPosition");
        public By CertDateElement => By.CssSelector("#txtCertDate");
        public By CertDateCalendarTextElement => By.CssSelector("#txtCertDate");
        public By CertDateCalendarElement => By.XPath("//div[@class='datepicker-days']//td[@class='day'][contains(text(),'17')]");
        public By CertDateMonthElement => By.XPath("//div[@class='datepicker-months']");
        public By CertDateDaysElement => By.XPath("//div[@class='datepicker-days']");
        public By CertDateYearElement => By.XPath("//div[@class='datepicker-years']");
        public By CertSubmitButtonElement => By.XPath("//form[@id='annualConfirmForm']//input[@name='btnSave']");


        //Program Tab Elements to be used for Annual Confirmation Tab
        public By ProgramsTabElement => By.XPath("//strong[contains(text(),'Programs')]");
        public By ProgramTabStatusElement => By.XPath("//span[@class='status program-tab-status']");
        public By ProgramTabTypeElement = (By.XPath("//tbody[@id='ProgramSummaryDataGrid']//td[contains(@class,'sorting_1')]"));
        public By ProgramTabTableElement => By.XPath("//div[@id='DataTables_Table_2_wrapper']");

        //Teachers Tab Elements to be used for Annual Confirmation Tab
        public By TeachersTabElement => By.XPath("//strong[contains(text(),'Teachers')]");
        public By TeachersTabStatusElement => By.XPath("//span[@class='status teacher-tab-status']");
        public By TeachersTabNooTeacherElement => By.CssSelector("#DataTables_Table_0_info");
        public By TeachersTabTableElement => By.XPath("//div[@id='DataTables_Table_0_wrapper']");

        //Other Educators Tab Elements to be used for Annual Confirmation Tab
        public By OtherEducatorsTabElement => By.XPath("//strong[contains(text(),'Other Educators')]");
        public By OtherEducatorsTabStatusElement => By.XPath("//span[@class='status assistant-tab-status']");
        public By OtherEducatorsTabNoofOtherEducatorsElement => By.CssSelector("#DataTables_Table_3_info");

        //Enrolments Tab Elements to be used for Annual Confirmation Tab
        public By EnrolmentsTabElement => By.CssSelector("a[href='#tab4']");
        public By EnrolmentsTabStatusElement => By.XPath("//span[@class='status enrolment-tab-status']");
        public By EnrolmentsTabNoofEnrolmentsElement => By.CssSelector("#DataTables_Table_4_info");

        //Other Annual Confirmation Form Elements
        public By DayCare_No_Radio => By.CssSelector("#isBeforeAfterCareOfferedNo");
        public IWebElement Checkbox1 => Driver.FindElement(By.CssSelector("#cnFECACEQA"));
        public IWebElement Checkbox2 => Driver.FindElement(By.CssSelector("#cnFECEnrolled"));
        public IWebElement Checkbox3 => Driver.FindElement(By.CssSelector("#cnFECWritten"));
        public By Insurace_No_radio => By.CssSelector("#rbcnIsAssociationIncNo");

        internal void CheckTEPEStatus()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //check the Teachers tab status
            wait.Until(x => x.FindElement(TeachersTabStatusElement));
            String strTeacherTabStatus = Driver.FindElement(TeachersTabStatusElement).Text;

            if (strTeacherTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Teacher tab status is " + strTeacherTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Teacher tab status is " + strTeacherTabStatus);
            };

            //check the Other Educators tab status
            wait.Until(x => x.FindElement(OtherEducatorsTabStatusElement));
            String strOtherEducatorsTabStatus = Driver.FindElement(OtherEducatorsTabStatusElement).Text;
            if (strOtherEducatorsTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Other Educators tab status is " + strOtherEducatorsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Other Educators tab status is " + strOtherEducatorsTabStatus);
            };

            //check the Programs tab status
            wait.Until(x => x.FindElement(ProgramTabStatusElement));
            String strProgramTabStatus = Driver.FindElement(ProgramTabStatusElement).Text;
            if (strProgramTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Programs tab status is " + strProgramTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Programs tab status is " + strProgramTabStatus);
            };

            //check the Enrolments tab status
            wait.Until(x => x.FindElement(EnrolmentsTabStatusElement));
            String strEnrolmentsTabStatus = Driver.FindElement(EnrolmentsTabStatusElement).Text;
            if (strEnrolmentsTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Enrolments tab status is " + strEnrolmentsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Enrolments tab status is " + strEnrolmentsTabStatus);
            };


        }

        internal void AnnualConfirmStatus()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //verify Annual Tab status
            wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
            String strAnnualConfirmationTabStatus = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
            if (strAnnualConfirmationTabStatus.Equals("Not complete"))
            {
                //throw new Exception("PASS: Annual Confirmation tab status is " + strAnnualConfirmationTabStatus);
            }
            else
            {
                throw new Exception("Fail: Annual Confirmation tab status is " + strAnnualConfirmationTabStatus);
            };
        }
        internal void AnnualConfirm()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(AnnualConfirmationTabElement));
            Driver.FindElement(AnnualConfirmationTabElement).Click();
            //AnnualConfirmationTabElement.Click();
        }

        internal void FieldsAnnualConfirmationTab()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(ProgramColumnHeaderElement));
            //get actual Program column header and compare with expected "Program" column header
            if (Driver.FindElement(ProgramColumnHeaderElement).GetAttribute("text").Contains("Program"))
            { }
            else
            {
                throw new Exception("Fail. Actual Program column header is " + Driver.FindElement(ProgramColumnHeaderElement).GetAttribute("text"));
            };

            //get actual Teachers column header and compare with expected "Teachers" column header
            if (Driver.FindElement(TeachersColumnHeaderElement).GetAttribute("text").Contains("Teachers"))
            { }
            else
            {
                throw new Exception("Fail. Actual Teacher column header is " + Driver.FindElement(TeachersColumnHeaderElement).GetAttribute("text"));
            };

            //get actual Other Educators column header and compare with expected "Other Educators" column header
            if (Driver.FindElement(OtherEducatorsColumnHeaderElement).GetAttribute("text").Contains("Other Educators"))
            { }
            else
            {
                throw new Exception("Fail. Actual Other Educators column header is " + Driver.FindElement(OtherEducatorsColumnHeaderElement).GetAttribute("text"));
            };

            //get actual Enrolments column header and compare with expected "Enrolments" column header
            if (Driver.FindElement(EnrolmentsColumnHeaderElement).GetAttribute("text").Contains("Enrolments"))
            { }
            else
            {
                throw new Exception("Fail. Actual Enrolments column header is " + Driver.FindElement(EnrolmentsColumnHeaderElement).GetAttribute("text"));
            };

            //get actual KFS Enrolments column header and compare with expected "KFS Enrolments" column header
            if (Driver.FindElement(KFSEnrolmentsColumnHeaderElement).GetAttribute("text").Contains("KFS Enrolments"))
            { }
            else
            {
                throw new Exception("Fail. Actual KFSEnrolments column header is " + Driver.FindElement(KFSEnrolmentsColumnHeaderElement).GetAttribute("text"));
            };

            /*wait.Until(x => x.FindElement(OptionsColumnHeaderElement));
            if (Driver.FindElement(OptionsColumnHeaderElement).GetAttribute("text").Contains("Options"))
            {}
                else
                {
                throw new Exception("Fail. Actual Options column header is " + Driver.FindElement(OptionsColumnHeaderElement).GetAttribute("text"));
                };*/

        }

        internal void ValuesAnnualConfirmationTab()
        {

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));


            //Checking value of Program Type in Programs Tab as the expected result
            wait.Until(x => x.FindElement(ProgramsTabElement));
            Driver.FindElement(ProgramsTabElement).Click();

            Thread.Sleep(2000);
            //wait.Until(x => x.FindElement(ProgramTypeElement));


            String strProgramTabType = Driver.FindElement(ProgramTabTypeElement).Text;

            //Checking value for number of teachers in Teachers Tab as the expected result
            wait.Until(x => x.FindElement(TeachersTabElement));
            Driver.FindElement(TeachersTabElement).Click();

            Thread.Sleep(2000);

            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            String strTeachersTabUntrimmedNoofTeachers = Driver.FindElement(TeachersTabNooTeacherElement).Text;
            String strFirstTrimNoofTeachers = strTeachersTabUntrimmedNoofTeachers.TrimEnd(charsToTrim);
            String strSecondTrimNoofTeachers = strFirstTrimNoofTeachers.Substring(strFirstTrimNoofTeachers.IndexOf("of") + 2);
            String strNoofTeachers = strSecondTrimNoofTeachers.Trim();

            /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
            TextWriter tmp = Console.Out;
            StreamWriter sw = new StreamWriter(fs);
            Console.SetOut(sw);
            Console.WriteLine(strNoofTeachers);
            Console.SetOut(tmp);
            sw.Close();
            */

            //Checking value for number of Educators in Other Educators Tab as the expected result
            wait.Until(x => x.FindElement(OtherEducatorsTabElement));
            Driver.FindElement(OtherEducatorsTabElement).Click();

            Thread.Sleep(2000);
            String strOtherEducatorsTabUntrimmedNoofTeachers = Driver.FindElement(OtherEducatorsTabNoofOtherEducatorsElement).Text;
            String strFirstTrimNoofEducators = strOtherEducatorsTabUntrimmedNoofTeachers.TrimEnd(charsToTrim);
            String strSecondTrimNoofEducators = strFirstTrimNoofEducators.Substring(strFirstTrimNoofEducators.IndexOf("of") + 2);
            String strNoofEducators = strSecondTrimNoofEducators.Trim();

            /*
             * FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
            TextWriter tmp = Console.Out;
            StreamWriter sw = new StreamWriter(fs);
            Console.SetOut(sw);
            Console.WriteLine("Educator:" +strNoofEducators);
            Console.SetOut(tmp);
            sw.Close();
            */

            //Checking value for number of Enrolments in Enrolments Tab as the expected result
            Thread.Sleep(8000);
            //wait.Until(x => x.FindElement(EnrolmentsTabElement));
            Driver.FindElement(EnrolmentsTabElement).Click();

            Thread.Sleep(2000);
            String strEnrolmentsTabUntrimmedNoofEnrolments = Driver.FindElement(EnrolmentsTabNoofEnrolmentsElement).Text;
            //throw new Exception(strEnrolmentsTabUntrimmedNoofEnrolments);
            String strFirstTrimNoofEnrolments = strEnrolmentsTabUntrimmedNoofEnrolments.TrimEnd(charsToTrim);
            String strSecondTrimNoofEnrolments = strFirstTrimNoofEnrolments.Substring(strFirstTrimNoofEnrolments.IndexOf("of") + 2);
            String strNoofEnrolments = strSecondTrimNoofEnrolments.Trim();

            /*FileStream fs = new FileStream("C:\\Users\\09976245\\Documents\\Test.txt", FileMode.Create);
            TextWriter tmp = Console.Out;
            StreamWriter sw = new StreamWriter(fs);
            Console.SetOut(sw);
            Console.WriteLine("Enrolments:" + strNoofnrolments);
            Console.SetOut(tmp);
            sw.Close();*/

            //Checking value for number of KFS Enrolments in Enrolments Tab as the expected result
            wait.Until(x => x.FindElement(EnrolmentsTabElement));
            Driver.FindElement(EnrolmentsTabElement).Click();

           
            int erTotalEnrolments = Int32.Parse(strNoofEnrolments);
            //int erTotalKFS = Int32.Parse(strARKFS);
            int nonKFS = Driver.FindElements(By.XPath("//tbody[@id='EnrolmentSummaryDataGrid']//td[contains(text(),'No')]")).Count;
            int kFS = erTotalEnrolments - nonKFS;//expected result KFS count


            //Checking the actual values in Annual Confirmation tab and comparing it the expected results

            //Checking Annual Confirmation for Program value
            Driver.FindElement(AnnualConfirmationTabElement).Click();
            Thread.Sleep(2000);
            wait.Until(x => x.FindElement(ProgramValueElement));
            if (Driver.FindElement(ProgramValueElement).Text.Equals(strProgramTabType))
            {
            }
            else
            {
                throw new Exception("FAIL - Annual Confirmation Tab's value for Program: " + Driver.FindElement(ProgramValueElement).Text + " is not the same as Program Tab's type: " + strProgramTabType);
            };
            //Checking Annual Confirmation for Teacher value
            if (Driver.FindElement(TeachersValueElement).Text.Equals(strNoofTeachers))
            {

            }
            else
            {
                throw new Exception("FAIL - Annual Confirmation Tab's value for Teachers: " + Driver.FindElement(TeachersValueElement).Text + " is not the same as Teachers Tab's value: " + strNoofTeachers);
            };

            //Checking Annual Confirmation for OtherEducators value
            if (Driver.FindElement(OtherEducatorsValueElement).Text.Equals(strNoofEducators))
            {

            }
            else
            {
                throw new Exception("FAIL - Annual Confirmation Tab's value for Other Educators: " + Driver.FindElement(OtherEducatorsValueElement).Text + " is not the same as Other Educators Tab's value: " + strNoofEducators);
            };

            //Checking Annual Confirmation for Enrolments value
            if (Driver.FindElement(EnrolmentsValueElement).Text.Equals(strNoofEnrolments))
            {

            }
            else
            {
                throw new Exception("FAIL - Annual Confirmation Tab's value for Enrolments: " + Driver.FindElement(EnrolmentsValueElement).Text + " is not the same as Enrolments Tab's value: " + strNoofEnrolments);
            };

            //Checking Annual Confirmation for KFSEnrolment value
            wait.Until(x => x.FindElement(KFSEnrolmentsValueElement));
            String strARKFS = Driver.FindElement(KFSEnrolmentsValueElement).Text;
            int arTotalKFS = Int32.Parse(strARKFS);
            if (arTotalKFS.Equals(kFS))
            { }
            else
            {
                throw new Exception("FAIL - Annual Confirmation Tab's value for KFS Enrolments: " + arTotalKFS + " is not the same as Enrolments Tab's value for KFS: " + kFS);
            };
            

        }

        internal void AlertforNOTComplete()
        {
            Thread.Sleep(2000);
            IAlert AnnualConfirmationAlert = Driver.SwitchTo().Alert();
            //aCAlert.Text();
            AnnualConfirmationAlert.Accept();
            Thread.Sleep(2000);
        }
        internal void ConfirmData()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(190));
            Thread.Sleep(6000);
            wait.Until(x => x.FindElement(ConfirmDataButtonElement));
            //throw new Exception ("Type is " + Driver.FindElement(ConfirmDataButtonElement).GetType());
            Driver.FindElement(ConfirmDataButtonElement).Click();
            //ConfirmDataElement.Click();
        }
        internal void FillAnnualConfirmationForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
                wait.Until(x => x.FindElement(CentralEnrolmentNoOptionElement));
                //click Central Enrolment NO (later add option with array YES/NO)
                Driver.FindElement(CentralEnrolmentNoOptionElement).Click();
                wait.Until(x => x.FindElement(TLDSOneElement));
                Driver.FindElement(TLDSOneElement).Clear();
                Driver.FindElement(TLDSOneElement).SendKeys(row.ItemArray[0].ToString());
                wait.Until(x => x.FindElement(TLDSTwoElement));
                Driver.FindElement(TLDSTwoElement).Clear();
                Driver.FindElement(TLDSTwoElement).SendKeys(row.ItemArray[1].ToString());
                wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsChildrenElement));
                Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).Clear();
                Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).SendKeys(row.ItemArray[2].ToString());
                wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement));
                Driver.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement).SendKeys(row.ItemArray[3].ToString());
                wait.Until(x => x.FindElement(DayCare_No_Radio));
                Driver.FindElement(DayCare_No_Radio).Click();
                wait.Until(x => x.FindElement(FeeSubsidyYesOptionElement));
                Driver.FindElement(FeeSubsidyYesOptionElement).Click();
                wait.Until(x => x.FindElement(FeeSubsidySurplusNoOptionElement));
                Driver.FindElement(FeeSubsidySurplusNoOptionElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();
                wait.Until(x => x.FindElement(TotalOtherEarlyElement));
                Driver.FindElement(TotalOtherEarlyElement).SendKeys(row.ItemArray[4].ToString());
                wait.Until(x => x.FindElement(MastersdegElement));
                Driver.FindElement(MastersdegElement).SendKeys(row.ItemArray[5].ToString());
                wait.Until(x => x.FindElement(GraduatedegElement));
                Driver.FindElement(GraduatedegElement).SendKeys(row.ItemArray[6].ToString());
                wait.Until(x => x.FindElement(DualBachelordegElement));
                Driver.FindElement(DualBachelordegElement).SendKeys(row.ItemArray[7].ToString());
                wait.Until(x => x.FindElement(BachelordegElement));
                Driver.FindElement(BachelordegElement).SendKeys(row.ItemArray[8].ToString());
                wait.Until(x => x.FindElement(PathwaydegElement));
                Driver.FindElement(PathwaydegElement).SendKeys(row.ItemArray[9].ToString());
                wait.Until(x => x.FindElement(DiplomadegElement));
                Driver.FindElement(DiplomadegElement).SendKeys(row.ItemArray[10].ToString());
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();

                wait.Until(x => x.FindElement(CertNameElement));
                Driver.FindElement(CertNameElement).SendKeys(row.ItemArray[11].ToString());
                wait.Until(x => x.FindElement(CertPositionElement));
                Driver.FindElement(CertPositionElement).SendKeys(row.ItemArray[12].ToString());

                //Selecting 17th of the current month
                //wait.Until(x => x.FindElement(CertDateElement));
                //Driver.FindElement(CertDateElement).SendKeys(row.ItemArray[13].ToString());
                Driver.FindElement(CertDateCalendarTextElement).Click();
                wait.Until(x => x.FindElement(CertDateCalendarElement));
                Driver.FindElement(CertDateCalendarElement).Click();
                wait.Until(x => x.FindElement(CertSubmitButtonElement));
                Driver.FindElement(CertSubmitButtonElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
                String strAnnualConfirmationTabStatusafter = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
                if (strAnnualConfirmationTabStatusafter.Equals("Submitted"))
                {
                }
                else
                {
                    throw new Exception("FAIL: Annual Confirm tab status is " + strAnnualConfirmationTabStatusafter);
                };
            }
        }

        internal void FillAnnualConfirmationForm_Sessional(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Thread.Sleep(2000);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
                wait.Until(x => x.FindElement(CentralEnrolmentNoOptionElement));
                //click Central Enrolment NO (later add option with array YES/NO)
                Driver.FindElement(CentralEnrolmentNoOptionElement).Click();
                wait.Until(x => x.FindElement(TLDSOneElement));
                Driver.FindElement(TLDSOneElement).Clear();
                Driver.FindElement(TLDSOneElement).SendKeys(row.ItemArray[0].ToString());
                wait.Until(x => x.FindElement(TLDSTwoElement));
                Driver.FindElement(TLDSTwoElement).Clear();
                Driver.FindElement(TLDSTwoElement).SendKeys(row.ItemArray[1].ToString());
                //wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsChildrenElement));
                //Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).Clear();
                //Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).SendKeys(row.ItemArray[2].ToString());
                //wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement));
                //Driver.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement).SendKeys(row.ItemArray[3].ToString());
                wait.Until(x => x.FindElement(DayCare_No_Radio));
                Driver.FindElement(DayCare_No_Radio).Click();
                wait.Until(x => x.FindElement(FeeSubsidyYesOptionElement));
                Driver.FindElement(FeeSubsidyYesOptionElement).Click();
                wait.Until(x => x.FindElement(FeeSubsidySurplusNoOptionElement));
                Driver.FindElement(FeeSubsidySurplusNoOptionElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();
                wait.Until(x => x.FindElement(TotalOtherEarlyElement));
                Driver.FindElement(TotalOtherEarlyElement).SendKeys(row.ItemArray[4].ToString());
                wait.Until(x => x.FindElement(MastersdegElement));
                Driver.FindElement(MastersdegElement).SendKeys(row.ItemArray[5].ToString());
                wait.Until(x => x.FindElement(GraduatedegElement));
                Driver.FindElement(GraduatedegElement).SendKeys(row.ItemArray[6].ToString());
                wait.Until(x => x.FindElement(DualBachelordegElement));
                Driver.FindElement(DualBachelordegElement).SendKeys(row.ItemArray[7].ToString());
                wait.Until(x => x.FindElement(BachelordegElement));
                Driver.FindElement(BachelordegElement).SendKeys(row.ItemArray[8].ToString());
                wait.Until(x => x.FindElement(PathwaydegElement));
                Driver.FindElement(PathwaydegElement).SendKeys(row.ItemArray[9].ToString());
                wait.Until(x => x.FindElement(DiplomadegElement));
                Driver.FindElement(DiplomadegElement).SendKeys(row.ItemArray[10].ToString());
                // Tick all checkboxes
                Thread.Sleep(2000);
                Checkbox1.Click();
                Thread.Sleep(2000);
                Checkbox2.Click();
                Thread.Sleep(2000);
                Checkbox3.Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();

                //insurance cover 
                wait.Until(x => x.FindElement(Insurace_No_radio));
                Driver.FindElement(Insurace_No_radio).Click();
                wait.Until(x => x.FindElement(CertNameElement));
                Driver.FindElement(CertNameElement).SendKeys(row.ItemArray[11].ToString());
                wait.Until(x => x.FindElement(CertPositionElement));
                Driver.FindElement(CertPositionElement).SendKeys(row.ItemArray[12].ToString());

                //Selecting 17th of the current month
                //wait.Until(x => x.FindElement(CertDateElement));
                //Driver.FindElement(CertDateElement).SendKeys(row.ItemArray[13].ToString());
                Driver.FindElement(CertDateCalendarTextElement).Click();
                wait.Until(x => x.FindElement(CertDateCalendarElement));
                Driver.FindElement(CertDateCalendarElement).Click();
                wait.Until(x => x.FindElement(CertSubmitButtonElement));
                Driver.FindElement(CertSubmitButtonElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
                String strAnnualConfirmationTabStatusafter = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
                if (strAnnualConfirmationTabStatusafter.Equals("Submitted"))
                {
                }
                else
                {
                    throw new Exception("FAIL: Annual Confirm tab status is " + strAnnualConfirmationTabStatusafter);
                };
            }
        }

        internal void FillAnnualConfirmationForm_Sessional_NoCheckbox(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Thread.Sleep(2000);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
                wait.Until(x => x.FindElement(CentralEnrolmentNoOptionElement));
                //click Central Enrolment NO (later add option with array YES/NO)
                Driver.FindElement(CentralEnrolmentNoOptionElement).Click();
                wait.Until(x => x.FindElement(TLDSOneElement));
                Driver.FindElement(TLDSOneElement).Clear();
                Driver.FindElement(TLDSOneElement).SendKeys(row.ItemArray[0].ToString());
                wait.Until(x => x.FindElement(TLDSTwoElement));
                Driver.FindElement(TLDSTwoElement).Clear();
                Driver.FindElement(TLDSTwoElement).SendKeys(row.ItemArray[1].ToString());
                //wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsChildrenElement));
                //Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).Clear();
                //Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).SendKeys(row.ItemArray[2].ToString());
                //wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement));
                //Driver.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement).SendKeys(row.ItemArray[3].ToString());
                wait.Until(x => x.FindElement(DayCare_No_Radio));
                Driver.FindElement(DayCare_No_Radio).Click();
                wait.Until(x => x.FindElement(FeeSubsidyYesOptionElement));
                Driver.FindElement(FeeSubsidyYesOptionElement).Click();
                wait.Until(x => x.FindElement(FeeSubsidySurplusNoOptionElement));
                Driver.FindElement(FeeSubsidySurplusNoOptionElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();
                wait.Until(x => x.FindElement(TotalOtherEarlyElement));
                Driver.FindElement(TotalOtherEarlyElement).SendKeys(row.ItemArray[4].ToString());
                wait.Until(x => x.FindElement(MastersdegElement));
                Driver.FindElement(MastersdegElement).SendKeys(row.ItemArray[5].ToString());
                wait.Until(x => x.FindElement(GraduatedegElement));
                Driver.FindElement(GraduatedegElement).SendKeys(row.ItemArray[6].ToString());
                wait.Until(x => x.FindElement(DualBachelordegElement));
                Driver.FindElement(DualBachelordegElement).SendKeys(row.ItemArray[7].ToString());
                wait.Until(x => x.FindElement(BachelordegElement));
                Driver.FindElement(BachelordegElement).SendKeys(row.ItemArray[8].ToString());
                wait.Until(x => x.FindElement(PathwaydegElement));
                Driver.FindElement(PathwaydegElement).SendKeys(row.ItemArray[9].ToString());
                wait.Until(x => x.FindElement(DiplomadegElement));
                Driver.FindElement(DiplomadegElement).SendKeys(row.ItemArray[10].ToString());
                // Do not tick all checkboxes
                Thread.Sleep(2000);
                //Checkbox1.Click();
                //Thread.Sleep(2000);
                //Checkbox2.Click();
                //Thread.Sleep(2000);
                //Checkbox3.Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();

                //insurance cover 
                wait.Until(x => x.FindElement(Insurace_No_radio));
                Driver.FindElement(Insurace_No_radio).Click();
                wait.Until(x => x.FindElement(CertNameElement));
                Driver.FindElement(CertNameElement).SendKeys(row.ItemArray[11].ToString());
                wait.Until(x => x.FindElement(CertPositionElement));
                Driver.FindElement(CertPositionElement).SendKeys(row.ItemArray[12].ToString());

                //Selecting 17th of the current month
                //wait.Until(x => x.FindElement(CertDateElement));
                //Driver.FindElement(CertDateElement).SendKeys(row.ItemArray[13].ToString());
                Driver.FindElement(CertDateCalendarTextElement).Click();
                wait.Until(x => x.FindElement(CertDateCalendarElement));
                Driver.FindElement(CertDateCalendarElement).Click();
                wait.Until(x => x.FindElement(CertSubmitButtonElement));
                Driver.FindElement(CertSubmitButtonElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
                String strAnnualConfirmationTabStatusafter = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
                if (strAnnualConfirmationTabStatusafter.Equals("Submitted"))
                {
                }
                else
                {
                    throw new Exception("FAIL: Annual Confirm tab status is " + strAnnualConfirmationTabStatusafter);
                };
            }
        }
        internal void FillAnnualConfirmationForm_SessionalSeparateCombined(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Thread.Sleep(2000);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
                wait.Until(x => x.FindElement(CentralEnrolmentNoOptionElement));
                //click Central Enrolment NO (later add option with array YES/NO)
                Driver.FindElement(CentralEnrolmentNoOptionElement).Click();
                wait.Until(x => x.FindElement(TLDSOneElement));
                Driver.FindElement(TLDSOneElement).Clear();
                Driver.FindElement(TLDSOneElement).SendKeys(row.ItemArray[0].ToString());
                wait.Until(x => x.FindElement(TLDSTwoElement));
                Driver.FindElement(TLDSTwoElement).Clear();
                Driver.FindElement(TLDSTwoElement).SendKeys(row.ItemArray[1].ToString());
                wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsChildrenElement));
                Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).Clear();
                Driver.FindElement(ThreeYrOldEnrolmentsChildrenElement).SendKeys(row.ItemArray[2].ToString());
                wait.Until(x => x.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement));
                Driver.FindElement(ThreeYrOldEnrolmentsHoursPerWeekElement).SendKeys(row.ItemArray[3].ToString());
                wait.Until(x => x.FindElement(DayCare_No_Radio));
                Driver.FindElement(DayCare_No_Radio).Click();
                wait.Until(x => x.FindElement(FeeSubsidyYesOptionElement));
                Driver.FindElement(FeeSubsidyYesOptionElement).Click();
                wait.Until(x => x.FindElement(FeeSubsidySurplusNoOptionElement));
                Driver.FindElement(FeeSubsidySurplusNoOptionElement).Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();
                wait.Until(x => x.FindElement(TotalOtherEarlyElement));
                Driver.FindElement(TotalOtherEarlyElement).SendKeys(row.ItemArray[4].ToString());
                wait.Until(x => x.FindElement(MastersdegElement));
                Driver.FindElement(MastersdegElement).SendKeys(row.ItemArray[5].ToString());
                wait.Until(x => x.FindElement(GraduatedegElement));
                Driver.FindElement(GraduatedegElement).SendKeys(row.ItemArray[6].ToString());
                wait.Until(x => x.FindElement(DualBachelordegElement));
                Driver.FindElement(DualBachelordegElement).SendKeys(row.ItemArray[7].ToString());
                wait.Until(x => x.FindElement(BachelordegElement));
                Driver.FindElement(BachelordegElement).SendKeys(row.ItemArray[8].ToString());
                wait.Until(x => x.FindElement(PathwaydegElement));
                Driver.FindElement(PathwaydegElement).SendKeys(row.ItemArray[9].ToString());
                wait.Until(x => x.FindElement(DiplomadegElement));
                Driver.FindElement(DiplomadegElement).SendKeys(row.ItemArray[10].ToString());
                // Tick all checkboxes
                Thread.Sleep(2000);
                Checkbox1.Click();
                Thread.Sleep(2000);
                Checkbox2.Click();
                Thread.Sleep(2000);
                Checkbox3.Click();
                wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
                Driver.FindElement(AnnualConfirmationNextButtonElement).Click();

                //insurance cover 
                wait.Until(x => x.FindElement(Insurace_No_radio));
                Driver.FindElement(Insurace_No_radio).Click();
                wait.Until(x => x.FindElement(CertNameElement));
                Driver.FindElement(CertNameElement).SendKeys(row.ItemArray[11].ToString());
                wait.Until(x => x.FindElement(CertPositionElement));
                Driver.FindElement(CertPositionElement).SendKeys(row.ItemArray[12].ToString());

                //Selecting 17th of the current month
                //wait.Until(x => x.FindElement(CertDateElement));
                //Driver.FindElement(CertDateElement).SendKeys(row.ItemArray[13].ToString());
                Driver.FindElement(CertDateCalendarTextElement).Click();
                wait.Until(x => x.FindElement(CertDateCalendarElement));
                Driver.FindElement(CertDateCalendarElement).Click();
                wait.Until(x => x.FindElement(CertSubmitButtonElement));
                Driver.FindElement(CertSubmitButtonElement).Click();
                Thread.Sleep(10000);
                wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
                String strAnnualConfirmationTabStatusafter = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
                if (strAnnualConfirmationTabStatusafter.Equals("Submitted"))
                {
                }
                else
                {
                    throw new Exception("FAIL: Annual Confirm tab status is " + strAnnualConfirmationTabStatusafter);
                };
            }
        }
        internal void CheckTEPEStatus_AfterAnnualConfirmationSubmit()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //check the Teachers tab status
            wait.Until(x => x.FindElement(TeachersTabStatusElement));
            String strTeacherTabStatus = Driver.FindElement(TeachersTabStatusElement).Text;

            if (strTeacherTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Teacher tab status is " + strTeacherTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Teacher tab status is " + strTeacherTabStatus);
            };

            //check the Other Educators tab status
            wait.Until(x => x.FindElement(OtherEducatorsTabStatusElement));
            String strOtherEducatorsTabStatus = Driver.FindElement(OtherEducatorsTabStatusElement).Text;
            if (strOtherEducatorsTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Other Educators tab status is " + strOtherEducatorsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Other Educators tab status is " + strOtherEducatorsTabStatus);
            };

            //check the Programs tab status
            wait.Until(x => x.FindElement(ProgramTabStatusElement));
            String strProgramTabStatus = Driver.FindElement(ProgramTabStatusElement).Text;
            if (strProgramTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Programs tab status is " + strProgramTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Programs tab status is " + strProgramTabStatus);
            };

            //check the Enrolments tab status
            wait.Until(x => x.FindElement(EnrolmentsTabStatusElement));
            String strEnrolmentsTabStatus = Driver.FindElement(EnrolmentsTabStatusElement).Text;
            if (strEnrolmentsTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Enrolments tab status is " + strEnrolmentsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Enrolments tab status is " + strEnrolmentsTabStatus);
            };
        }
        internal void ViewData()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(190));
            Thread.Sleep(6000);
            wait.Until(x => x.FindElement(ViewDataButtonElement));
            Driver.FindElement(ViewDataButtonElement).Click();
        }
        internal void ViewAnnualConfirmationForm()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(AnnualConfirmFormElement));            
            wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
            Driver.FindElement(AnnualConfirmationNextButtonElement).Click();
            wait.Until(x => x.FindElement(AnnualConfirmationNextButtonElement));
            Driver.FindElement(AnnualConfirmationNextButtonElement).Click();
            wait.Until(x => x.FindElement(AnnualConfirmationPreviousButtonElement));
            Driver.FindElement(AnnualConfirmationPreviousButtonElement).Click();
            wait.Until(x => x.FindElement(AnnualConfirmationExitButtonElement));
            Driver.FindElement(AnnualConfirmationExitButtonElement).Click();
        }
    }

}
