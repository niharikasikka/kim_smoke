﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace KIM_SmokeTest.Pages
{
    class Kim1Homepage : BaseApplicationPage
    {
        public Kim1Homepage(IWebDriver driver) : base(driver)
        { }
        public By ActualText_Element => By.XPath("//h1[contains(text(),'Kindergarten Information Management')]");
        public IWebElement UserId_Element => Driver.FindElement(By.CssSelector("#username"));
        public IWebElement Password_Element => Driver.FindElement(By.XPath("//input[@id='password']"));
        public IWebElement Submit_Element => Driver.FindElement(By.XPath("//input[@id='SubmitCreds']"));
        public IWebElement ServiceProvider_Element => Driver.FindElement(By.XPath("//a[contains(text(),'Service Providers')]"));
        public IWebElement LogOutMenu_Element => Driver.FindElement(By.XPath("//span[@id='zz16_Menu_t']"));
        public IWebElement LogOut_Element =>  Driver.FindElement(By.XPath("//a[@id='mp1_0_1_Anchor']"));
        public IWebElement Reports_Element => Driver.FindElement(By.XPath("//a[contains(text(),'Reports')]"));
        
        internal string GetUrl()
        {
            //Navigate to url with or without authentication alert/pop up
            string env= CommonFunctions.GetResourceValue("Env");
            string url = "";
            if (env.Equals("dev"))
            {
                url = CommonFunctions.GetResourceValue("Kim1Url_dev");
                return url;

            }
            else if (env.Equals("test"))
            {
                url = CommonFunctions.GetResourceValue("Kim1Url_test");
                return url;
            }
            else if (env.Equals("stage"))
            {
                url = CommonFunctions.GetResourceValue("Kim1Url_stage");
                return url;
            }
            else if (env.Equals("prod"))
            {
                url = CommonFunctions.GetResourceValue("Kim1Url_prod");
                return url;
            }
            else
            {
                //((IJavaScriptExecutor)Driver).ExecuteScript(string.Format("setTimeout(function() {{ location.href = \"{0}\" }}, 150);", url));

                //Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(80);
                //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(80);
                return url;
            }
        }

        internal void EnterUsername()
        {
            string userName = CommonFunctions.GetResourceValue("Kim1UserId_Internal");
            //Enter user id of the user
            UserId_Element.SendKeys(userName);
            
        }

        internal void SignIn(string url)
        {
            string userName;
            string password;
            string userType = CommonFunctions.GetResourceValue("Kim_UserType");
            if (userType.Equals("internal"))
            {
                 password = CommonFunctions.GetResourceValue("Kim1Password_Internal");
                 userName = CommonFunctions.GetResourceValue("Kim1UserId_Internal");
                Driver.Navigate().GoToUrl("https://" + userName + ":" + password + "@" + url);
            }
           else if (userType.Equals("external"))
            {
                 userName = CommonFunctions.GetResourceValue("Kim1_ExternalUser_Id");
                 password = CommonFunctions.GetResourceValue("KIm1_ExternalUser_Password");
                Driver.Navigate().GoToUrl("https://" + userName + ":" + password + "@" + url);
            }

           
        }
        internal void EnterPassword()
        {

            string password = CommonFunctions.GetResourceValue("Kim1Password_Internal");
            Password_Element.SendKeys(password);  
        }

        internal void ClickButton()
        { 
            Submit_Element.Click();
        }

        internal void LoggedIn()
        {
            WebDriverWait waitAgency = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
            waitAgency.Until(c => c.FindElement(ActualText_Element));
            Assert.AreEqual("Kindergarten Information Management", Driver.FindElement(ActualText_Element).Text.ToString());
        }

        internal void ServiceProviderClick()
        {
            string userType = CommonFunctions.GetResourceValue("Kim_UserType");
            if (userType.Equals("internal"))
            {
                ServiceProvider_Element.Click();
            }
            else
            { }
        }

        internal void ClickReports()
        {
            Reports_Element.Click();
            Task.Delay(10000).Wait();
        }
    }
}